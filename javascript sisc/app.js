const button = document.getElementById("enter");
const input = document.getElementById("userInput");
const ul = document.querySelector("ul");
const butonfiltrare = document.getElementsByClassName("filter");
const dropdown = document.getElementById("dropdown");

function inputLength() {
  return input.value.length;
}

function createListElement() {
  const li = document.createElement("li");
  const span = document.createElement("span");
  const icon = document.createElement("i");
  icon.classList.add("fas", "fa-trash"); //mai multe clase
  span.className = "shopping-item-name"; //o clasa
  span.textContent = input.value;
  li.appendChild(span);
  li.classList.add("filterDiv", dropdown.value);
  li.appendChild(icon);
  ul.appendChild(li);
  input.value = "";
}
function deleteListElement(target) {
  //target.ParentElement
  ul.removeChild(target.parentElement);
}
function addListItemAfterClick() {
  if (inputLength() > 0) {
    createListElement();
  }
}
function addListItemAfterKeypress(event) {
  if (inputLength() > 0 && event.key == "Enter") {
    createListElement();
  }
}
button.addEventListener("click", addListItemAfterClick);
input.addEventListener("keypress", addListItemAfterKeypress);
document.addEventListener("click", (event) => {
  if (event.target.classList.contains("fa-trash")) {
    deleteListElement(event.target);
  } else if (event.target.classList.contains("shopping-item-name")) {
    event.target.classList.toggle("done");
  } else {
    return;
  }
});

//n-am reusit singur, codul l am luat de pe w3school, eu doar l am schimbat putin, e dupa voi daca luati in considerare
//https://www.w3schools.com/howto/howto_js_filter_elements.asp

filterSelection("all");
function filterSelection(c) {
  var x, i;
  x = document.getElementsByClassName("filterDiv");
  if (c == "all") c = "";
  for (i = 0; i < x.length; i++) {
    removeClass(x[i], "filterx");
    if (x[i].className.indexOf(c) > -1) addClass(x[i], "filterx");
  }
}
function addClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    if (arr1.indexOf(arr2[i]) == -1) {
      element.className += " " + arr2[i];
    }
  }
}
function removeClass(element, name) {
  var i, arr1, arr2;
  arr1 = element.className.split(" ");
  arr2 = name.split(" ");
  for (i = 0; i < arr2.length; i++) {
    while (arr1.indexOf(arr2[i]) > -1) {
      arr1.splice(arr1.indexOf(arr2[i]), 1);
    }
  }
  element.className = arr1.join(" ");
}
var btnContainer = document.getElementsByClassName("containerb");
var btns = document.getElementsByClassName("filterbtn");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function () {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}

//incercare de persistent storage
// local
myStorage = window.localStorage;
var li = document.getElementsByClassName("filterDiv");
var spans = document.getElementsByClassName("shopping-item-name");
var is = document.getElementsByClassName("fas", "fa-trash");
var ts = document.querySelectorAll("span").forEach.textContent; //chiar nu stiu cum sa fac aici
var saved = new Object();
saved.container = li;
saved.spans = spans;
saved.icons = is;
saved.text = ts; //imi da bine aici, am adaugat eu spans, is si ts poate merge asa fragmentat dar nu mi-a iesit sa bag in stringify, gen obiectul se schimba cand adauci/stergi si samd
var savedString = JSON.stringify(saved); //si nu mi da bine aici
localStorage.setItem("savedstuff", savedString);
var lsg = localStorage.getItem("savedstuff");
// astfel nu pot sa fac persistent storage
